import { Item } from "./Item";
import { Character } from "./Character";


export class Potion extends Item {
    /**
     * On construit la classe Potion avec les mêmes paramètres que ceux de la classe Item
     * @param {string} name le nom de la potion
     * @param {number} x position x de la potion
     * @param {number} y position y de la potion
     * @param {number} healing les calories de la potion
     */
    constructor(name, x, y, healing) {
        super(name, x, y, 'potion');
        this.healing = healing;
    }

    /**
     * @override
     * @param {Character} character
     */
    use(character) {
        if (character.health < 100) {
            character.health += this.healing;
            if (character.health + this.healing > 100) {
                character.health = 100;
            }
            console.log(`${character.name} takes ${this.name} and his health is now ${character.health}.`);
            return super.use(character);

        } else {
            console.log(`Your health is already maximum !`);
        }
    }
}