import { Character } from "../Character";

export class ArrowKey {

    /**
     * @param {Character} character
     */
    // The new property of the ArrowKey class is an instance of Character :

    constructor(character) {
        this.character = character;
    }

    init() {
        window.addEventListener('keydown', event => {
            if (event.keyCode === 38) {
                this.character.move('up');
            } else if (event.keyCode === 40) {
                this.character.move('down');
            } else if (event.keyCode === 37) {
                this.character.move('left');
            } else if (event.keyCode === 39) {
                this.character.move('right');
            }
            // Touche espace pour prendre un objet
            if (event.keyCode === 32) {
                this.character.takeItem();
                // console.log(this.character.backpack);
            }
            // Touche i pour afficher inventaire
            if (event.keyCode === 73) {
                this.character.toggleBackpack();

            }

        });
    }
}