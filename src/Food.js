import { Item } from "./Item";
import { Character } from "./Character";

// on utilise le mot-clé 'extends' pour dire que la classe Food hérite de la classe Item

export class Food extends Item {
    /**
     * On construit la classe Food avec les mêmes paramètres que ceux de la classe Item
     * @param {string} name le nom de la nourriture
     * @param {number} x position x de la nourriture
     * @param {number} y position y de la nourriture
     * @param {number} calories les calories de la nourriture
     */
    constructor(name, x, y, calories) {
        // AVANT TOUT il faut déclencher le constructeur du parent (ici Item) avec le mot clé 'super'
        super(name, x, y, 'food');
        this.calories = calories;
    }


    // La classe food hérite des méthode de sa classe parent (Item) 
    // et on peut modifier la méthode appelée spécifiquement 
    // pour la classe food (override = redéfinition de la méthode)
    // C'est de la POLYMORPHIE 
    /**
     * @override
     * @param {Character} character
     */
    use(character) {
        if (character.hunger == 0) {
            console.log(`${character.name} is not hungry for now !`);
            return;
        }
        character.hunger -= this.calories;

        if (character.hunger - this.calories < 0) {
            character.hunger = 0;
        }
        console.log(`${character.name} eats ${this.name} and his hunger is now ${character.hunger}`);

        // et on rappelle la méthode du parent pour qu'elle soit aussi exécutée
        return super.use(character);
    }
}