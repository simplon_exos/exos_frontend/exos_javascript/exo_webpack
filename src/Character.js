import { tileSize, gameObjects } from ".";
import { Item } from "./Item";

/**
 * Pour que cette classe soit importable dans un autre
 * fichier, on doit l'indiquer avec le mot clef export
 */
export class Character {

    /**
     * @param {string} name 
     */
    constructor(name) {
        this.name = name;
        this.health = 100;
        this.hunger = 0;
        this.position = {
            x: 0,
            y: 0
        };
        this.element = null;
        /**
         * @type Item[]
         */
        this.backpack = [];
        this.showBackpack = false;
    }

    /**
     * Méthode qui ouvre ou ferme le backpack du personnage
     */
    toggleBackpack() {
        this.showBackpack = !this.showBackpack;
        this.draw();

    }

    /**
     * Method which is mooving the character in a specific direction
     * @param {string} direction the direction where we want to moove (must be 'up', 'down', 'left' ou 'right')
     */
    move(direction) {
        // 2 ways to make the condition loop for the movment (with 4 'if' or):
        switch (direction) {
            case 'up':
                this.position.y--;
                break;
            case 'down':
                this.position.y++;
                break;
            case 'left':
                this.position.x--;
                break;
            case 'right':
                this.position.x++;
                break;
        }
        this.metabolism();
        this.draw();
    }


    /**
     * Method to make that when the character moves, hunger decrement of 2
     * and if hunger is up to 100, health will decrement.
     */
    metabolism() {
        if (this.hunger < 100) {
            this.hunger += 2;
        } else {
            this.health--;
        }
    }


    /**
     * Méthode qui attendra un item en argument et qui l'ajoutera dans le tableau backpack
     * @param {Item} item l'item a mettre dans le sac
     */

    takeItem(item) {
        // let item;
        if (!item) {
            for (const object of gameObjects) {
                if (object instanceof Item) {
                    if (this.position.x == object.position.x &&
                        this.position.y == object.position.y) {
                        item = object;
                        break;
                    }
                }
            }
        }
        if (item) {
            this.backpack.push(item);
            // pour enlever l'item on doit mettre sa position à null (sinon il disparait mais garde ses coordonnées)
            item.position.x = null;
            item.position.y = null;
            item.element.remove();
            console.log(`${this.name} takes ${item.name}`);
        }
    }

    /**
     * Méthode permettant au personnage d'utiliser un item
     * @param {Item} item L'item à utiliser
     */
    useItem(item) {
        item.use(this);
    }


    /**
     * Méthode qui crée l'élément HTML du personnage
     * @returns {Element} l'élément HTML représentant le personnage
     */
    draw() {
        // If element is empty, we create it :
        if (!this.element) {
            this.element = document.createElement('div');
            this.element.classList.add('character-style');
        } // Else, we just have to empty the previous one :
        else {
            this.element = document.querySelector('.character-style');
            this.element.innerHTML = '';
        }

        // Make the character's parameters appear in it :
        this.drawStats();

        // Condition qui ouvre le backpack si showBackpack est true
        if (this.showBackpack) {
            this.openBackpack();
        }

        // The character moves in order of its size
        this.element.style.top = this.position.y * tileSize + 'px';
        this.element.style.left = this.position.x * tileSize + 'px';

        return this.element;

    }

    /**
     * Méthode qui crée le HTML des statistiques du personnage
     */
    drawStats() {
        let pName = document.createElement('p');
        pName.textContent = 'Name : ' + this.name;
        this.element.appendChild(pName);
        let pHealth = document.createElement('p');
        pHealth.textContent = 'Health : ' + this.health;
        this.element.appendChild(pHealth);
        let pHunger = document.createElement('p');
        pHunger.textContent = 'Hunger : ' + this.hunger;
        this.element.appendChild(pHunger);
    }

    openBackpack() {
        let backpack = document.createElement('div');
        backpack.classList.add('backpack');
        for (const item of this.backpack) {
            // Méthode pour dessiner l'élément HTML du backpack
            // On peut faire de 2 façons
            let paraItem = document.createElement('p');
            paraItem.textContent = `${item.name}`;
            paraItem.addEventListener('click', e => this.useItem(item));
            backpack.appendChild(paraItem);
            this.element.appendChild(backpack);

            // 2e methode : a verifier
            // backpack.appendChild(item.draw());
        }
    }
}