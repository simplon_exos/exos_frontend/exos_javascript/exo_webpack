/**
 * En JS modulaire, pour pouvoir utiliser la classe Character
 * dans notre fichier index, on doit d'abord importer la
 * classe en question de son fichier
 */
import { Character } from "./Character";
import { ArrowKey } from "./controls/ArrowKey";
import { Item } from "./Item";
import { Food } from "./Food";
import { Potion } from "./Potion";

export const tileSize = 100;

//On fait une instance de personnage
let perso1 = new Character('Link');

// On met dans un taleau toutes les instances créees (Character & Item)
export const gameObjects = [
    perso1,
    new Potion('Health Potion', 2, 2, 20),
    new Food('Magic Taco', 5, 5, 50),
    new Item('Shoes', 6, 3, 'shoes')
];
// On fait une boucle pour initialiser le draw de toutes les instances
for (const iterator of gameObjects) {
    document.body.appendChild(iterator.draw());

}

//on fait une instance des contrôles au clavier en lui
//donnant l'instance de personnage à contrôler
let arrowKey = new ArrowKey(perso1);
//On initialise les contrôles au clavier
arrowKey.init();

// Test pour la méthode .takeItem de Character
// perso1.takeItem();

// Test pour la méthode .useItem de Character
// perso1.useItem(new Item('some stuff'));

// // Test pour Potion
// gameObjects[1].use(perso1);

// // Test pour Food
// gameObjects[2].use(perso1);