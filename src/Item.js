import { Character } from "./Character";
import { tileSize } from ".";

export class Item {
    /**
     * 
     * @param {string} name le nom de l'item
     * @param {number} x position x de l'item
     * @param {number} y position y de l'item
     * @param {string} cssClass la classe css représentant cet item
     */
    constructor(name, x, y, cssClass = 'default-class') {
            this.name = name;
            this.position = {
                x,
                y
            };
            this.cssClass = cssClass;
            /**
             * @type Element
             */
            this.element = null;
        }
        /**
         * Méthode qui sera appelée quand on utilise l'item
         * @param {Character} character le personnage qui utilise l'item 
         * @returns {string} La chaine de caractères indiquant l'utilisation de l'item
         */
    use(character) {
        console.log(`${character.name} used ${this.name} !`);
    }

    /**
     * Méthode permettant de desisner l'élément sauf s'il existe déja
     * @returns {div} l'élément HTML représentatn l'Item
     */
    draw() {
        if (!this.element) {
            this.element = document.createElement('div');
        }
        let div = this.element;
        div.innerHTML = '';
        div.classList.add('item');
        div.classList.add(this.cssClass);
        div.textContent = '';

        div.style.top = this.position.y * tileSize + 'px';
        div.style.left = this.position.x * tileSize + 'px';

        return div;

    }

}