Vous pouvez aller sur la branche boilerplate pour avoir un début de projet vierge

## How To Use
1. `git clone https://gitlab.com/simplonlyon/promo12/js-exercise-webpack.git`
2. `cd js-exercise-webpack`
3. `npm install`
4. `npm run dev`

## Exercice POO / Webpack
### I. Mise en place
1. Créer un nouveau projet npm js-exercice-webpack
2. Dans ce projet, créer le structure de dossier standard, installer webpack et créer un fichier de configuration avec juste ce qu'il faut pour qu'il soit en mode development avec les source-maps qui vont bien
3. Créer un script dev dans le package.json qui lancera la compilation webpack en mode watch
4. Créer votre point d'entrée js, votre fichier HTML et charger le bundle dedans
### II. La classe Character
1. Créer un fichier contenant une classe Character ayant comme propriétés : health (number), hunger (number), name (string) et une propriété position qui sera un objet avec x (number) et y (number) initialisés à zéro
2. Créer une méthode move(direction) qui selon si elle reçoit 'up', 'down', 'left' ou 'right' modifiera la position x/y de l'instance
3. Créer une méthode draw qui générera le HTML du personnage, comme on l'a déjà fait, à vous de voir à quoi vous voulez qu'il ressemble (pourquoi pas un carré rouge ?), dans tous les cas il devra afficher ses stat en dessous de lui, sera en position absolute et aura sa position y en top et sa position x en left
4. Faire une instance de Character dans le index.js, déclencher sa méthode draw et sa méthode move



### III. Automatiser le draw

1. Dans la classe Character, rajouter une propriété element avec null en valeur par défaut

2. Modifier la méthode draw pour faire que si la propriété element est null, il fait le createElement et le met dans la propriété, sinon faire qu'il remette à zéro le innerHTML de la propriété element

3. Faire un appel de la méthode draw() à la fin de la méthode move()



### IV. Contrôler le personnage

1. Créer un dossier src/controls et dedans créer un fichier ArrowKey.js

2. Dans ce fichier, créer (et exporter) une classe ArrowKey qui aura un Character en propriété

3. Créer une méthode init dans cette classe qui ajoutera un event listener au keydown sur l'élément window. (attention, on devra utiliser this dans cet event listener, donc bien faire une fat arrow function)

4. Dans l'event listener, utiliser l'argument `event` de la fonction pour faire en sorte de déclencher la méthode move de la propriété character avec l'argument qui va bien selon la touche qui a été pressée. *en gros il va falloir récupérer la valeur de `event.key` ou `event.code` et via des if, déclencher la méthode this.character.move() en lui donnant 'up' si jamais le event.key est ArrowUp, ou down si ArrowDown etc.*

5. Dans le index.js importer la classe ArrowKey et en faire une instance en lui donnant notre instance en argument, puis lancer sa méthode init()

