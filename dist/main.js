/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/Character.js":
/*!**************************!*\
  !*** ./src/Character.js ***!
  \**************************/
/*! exports provided: Character */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Character", function() { return Character; });
/* harmony import */ var ___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! . */ "./src/index.js");
/* harmony import */ var _Item__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Item */ "./src/Item.js");



/**
 * Pour que cette classe soit importable dans un autre
 * fichier, on doit l'indiquer avec le mot clef export
 */
class Character {

    /**
     * @param {string} name 
     */
    constructor(name) {
        this.name = name;
        this.health = 100;
        this.hunger = 0;
        this.position = {
            x: 0,
            y: 0
        };
        this.element = null;
        /**
         * @type Item[]
         */
        this.backpack = [];
        this.showBackpack = false;
    }

    /**
     * Méthode qui ouvre ou ferme le backpack du personnage
     */
    toggleBackpack() {
        this.showBackpack = !this.showBackpack;
        this.draw();

    }

    /**
     * Method which is mooving the character in a specific direction
     * @param {string} direction the direction where we want to moove (must be 'up', 'down', 'left' ou 'right')
     */
    move(direction) {
        // 2 ways to make the condition loop for the movment (with 4 'if' or):
        switch (direction) {
            case 'up':
                this.position.y--;
                break;
            case 'down':
                this.position.y++;
                break;
            case 'left':
                this.position.x--;
                break;
            case 'right':
                this.position.x++;
                break;
        }
        this.metabolism();
        this.draw();
    }


    /**
     * Method to make that when the character moves, hunger decrement of 2
     * and if hunger is up to 100, health will decrement.
     */
    metabolism() {
        if (this.hunger < 100) {
            this.hunger += 2;
        } else {
            this.health--;
        }
    }


    /**
     * Méthode qui attendra un item en argument et qui l'ajoutera dans le tableau backpack
     * @param {Item} item l'item a mettre dans le sac
     */

    takeItem(item) {
        // let item;
        if (!item) {
            for (const object of ___WEBPACK_IMPORTED_MODULE_0__["gameObjects"]) {
                if (object instanceof _Item__WEBPACK_IMPORTED_MODULE_1__["Item"]) {
                    if (this.position.x == object.position.x &&
                        this.position.y == object.position.y) {
                        item = object;
                        break;
                    }
                }
            }
        }
        if (item) {
            this.backpack.push(item);
            // pour enlever l'item on doit mettre sa position à null (sinon il disparait mais garde ses coordonnées)
            item.position.x = null;
            item.position.y = null;
            item.element.remove();
            console.log(`${this.name} takes ${item.name}`);
        }
    }

    /**
     * Méthode permettant au personnage d'utiliser un item
     * @param {Item} item L'item à utiliser
     */
    useItem(item) {
        item.use(this);
    }


    /**
     * Méthode qui crée l'élément HTML du personnage
     * @returns {Element} l'élément HTML représentant le personnage
     */
    draw() {
        // If element is empty, we create it :
        if (!this.element) {
            this.element = document.createElement('div');
            this.element.classList.add('character-style');
        } // Else, we just have to empty the previous one :
        else {
            this.element = document.querySelector('.character-style');
            this.element.innerHTML = '';
        }

        // Make the character's parameters appear in it :
        this.drawStats();

        // Condition qui ouvre le backpack si showBackpack est true
        if (this.showBackpack) {
            this.openBackpack();
        }

        // The character moves in order of its size
        this.element.style.top = this.position.y * ___WEBPACK_IMPORTED_MODULE_0__["tileSize"] + 'px';
        this.element.style.left = this.position.x * ___WEBPACK_IMPORTED_MODULE_0__["tileSize"] + 'px';

        return this.element;

    }

    /**
     * Méthode qui crée le HTML des statistiques du personnage
     */
    drawStats() {
        let pName = document.createElement('p');
        pName.textContent = 'Name : ' + this.name;
        this.element.appendChild(pName);
        let pHealth = document.createElement('p');
        pHealth.textContent = 'Health : ' + this.health;
        this.element.appendChild(pHealth);
        let pHunger = document.createElement('p');
        pHunger.textContent = 'Hunger : ' + this.hunger;
        this.element.appendChild(pHunger);
    }

    openBackpack() {
        let backpack = document.createElement('div');
        backpack.classList.add('backpack');
        for (const item of this.backpack) {
            // Méthode pour dessiner l'élément HTML du backpack
            // On peut faire de 2 façons
            let paraItem = document.createElement('p');
            paraItem.textContent = `${item.name}`;
            paraItem.addEventListener('click', e => this.useItem(item));
            backpack.appendChild(paraItem);
            this.element.appendChild(backpack);

            // 2e methode : a verifier
            // backpack.appendChild(item.draw());
        }
    }
}

/***/ }),

/***/ "./src/Food.js":
/*!*********************!*\
  !*** ./src/Food.js ***!
  \*********************/
/*! exports provided: Food */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Food", function() { return Food; });
/* harmony import */ var _Item__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Item */ "./src/Item.js");
/* harmony import */ var _Character__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Character */ "./src/Character.js");



// on utilise le mot-clé 'extends' pour dire que la classe Food hérite de la classe Item

class Food extends _Item__WEBPACK_IMPORTED_MODULE_0__["Item"] {
    /**
     * On construit la classe Food avec les mêmes paramètres que ceux de la classe Item
     * @param {string} name le nom de la nourriture
     * @param {number} x position x de la nourriture
     * @param {number} y position y de la nourriture
     * @param {number} calories les calories de la nourriture
     */
    constructor(name, x, y, calories) {
        // AVANT TOUT il faut déclencher le constructeur du parent (ici Item) avec le mot clé 'super'
        super(name, x, y, 'food');
        this.calories = calories;
    }


    // La classe food hérite des méthode de sa classe parent (Item) 
    // et on peut modifier la méthode appelée spécifiquement 
    // pour la classe food (override = redéfinition de la méthode)
    // C'est de la POLYMORPHIE 
    /**
     * @override
     * @param {Character} character
     */
    use(character) {
        if (character.hunger == 0) {
            console.log(`${character.name} is not hungry for now !`);
            return;
        }
        character.hunger -= this.calories;

        if (character.hunger - this.calories < 0) {
            character.hunger = 0;
        }
        console.log(`${character.name} eats ${this.name} and his hunger is now ${character.hunger}`);

        // et on rappelle la méthode du parent pour qu'elle soit aussi exécutée
        return super.use(character);
    }
}

/***/ }),

/***/ "./src/Item.js":
/*!*********************!*\
  !*** ./src/Item.js ***!
  \*********************/
/*! exports provided: Item */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Item", function() { return Item; });
/* harmony import */ var _Character__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Character */ "./src/Character.js");
/* harmony import */ var ___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! . */ "./src/index.js");



class Item {
    /**
     * 
     * @param {string} name le nom de l'item
     * @param {number} x position x de l'item
     * @param {number} y position y de l'item
     * @param {string} cssClass la classe css représentant cet item
     */
    constructor(name, x, y, cssClass = 'default-class') {
            this.name = name;
            this.position = {
                x,
                y
            };
            this.cssClass = cssClass;
            /**
             * @type Element
             */
            this.element = null;
        }
        /**
         * Méthode qui sera appelée quand on utilise l'item
         * @param {Character} character le personnage qui utilise l'item 
         * @returns {string} La chaine de caractères indiquant l'utilisation de l'item
         */
    use(character) {
        console.log(`${character.name} used ${this.name} !`);
    }

    /**
     * Méthode permettant de desisner l'élément sauf s'il existe déja
     * @returns {div} l'élément HTML représentatn l'Item
     */
    draw() {
        if (!this.element) {
            this.element = document.createElement('div');
        }
        let div = this.element;
        div.innerHTML = '';
        div.classList.add('item');
        div.classList.add(this.cssClass);
        div.textContent = '';

        div.style.top = this.position.y * ___WEBPACK_IMPORTED_MODULE_1__["tileSize"] + 'px';
        div.style.left = this.position.x * ___WEBPACK_IMPORTED_MODULE_1__["tileSize"] + 'px';

        return div;

    }

}

/***/ }),

/***/ "./src/Potion.js":
/*!***********************!*\
  !*** ./src/Potion.js ***!
  \***********************/
/*! exports provided: Potion */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Potion", function() { return Potion; });
/* harmony import */ var _Item__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Item */ "./src/Item.js");
/* harmony import */ var _Character__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Character */ "./src/Character.js");




class Potion extends _Item__WEBPACK_IMPORTED_MODULE_0__["Item"] {
    /**
     * On construit la classe Potion avec les mêmes paramètres que ceux de la classe Item
     * @param {string} name le nom de la potion
     * @param {number} x position x de la potion
     * @param {number} y position y de la potion
     * @param {number} healing les calories de la potion
     */
    constructor(name, x, y, healing) {
        super(name, x, y, 'potion');
        this.healing = healing;
    }

    /**
     * @override
     * @param {Character} character
     */
    use(character) {
        if (character.health < 100) {
            character.health += this.healing;
            if (character.health + this.healing > 100) {
                character.health = 100;
            }
            console.log(`${character.name} takes ${this.name} and his health is now ${character.health}.`);
            return super.use(character);

        } else {
            console.log(`Your health is already maximum !`);
        }
    }
}

/***/ }),

/***/ "./src/controls/ArrowKey.js":
/*!**********************************!*\
  !*** ./src/controls/ArrowKey.js ***!
  \**********************************/
/*! exports provided: ArrowKey */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ArrowKey", function() { return ArrowKey; });
/* harmony import */ var _Character__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../Character */ "./src/Character.js");


class ArrowKey {

    /**
     * @param {Character} character
     */
    // The new property of the ArrowKey class is an instance of Character :

    constructor(character) {
        this.character = character;
    }

    init() {
        window.addEventListener('keydown', event => {
            if (event.keyCode === 38) {
                this.character.move('up');
            } else if (event.keyCode === 40) {
                this.character.move('down');
            } else if (event.keyCode === 37) {
                this.character.move('left');
            } else if (event.keyCode === 39) {
                this.character.move('right');
            }
            // Touche espace pour prendre un objet
            if (event.keyCode === 32) {
                this.character.takeItem();
                // console.log(this.character.backpack);
            }
            // Touche i pour afficher inventaire
            if (event.keyCode === 73) {
                this.character.toggleBackpack();

            }

        });
    }
}

/***/ }),

/***/ "./src/index.js":
/*!**********************!*\
  !*** ./src/index.js ***!
  \**********************/
/*! exports provided: tileSize, gameObjects */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "tileSize", function() { return tileSize; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "gameObjects", function() { return gameObjects; });
/* harmony import */ var _Character__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Character */ "./src/Character.js");
/* harmony import */ var _controls_ArrowKey__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./controls/ArrowKey */ "./src/controls/ArrowKey.js");
/* harmony import */ var _Item__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./Item */ "./src/Item.js");
/* harmony import */ var _Food__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./Food */ "./src/Food.js");
/* harmony import */ var _Potion__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./Potion */ "./src/Potion.js");
/**
 * En JS modulaire, pour pouvoir utiliser la classe Character
 * dans notre fichier index, on doit d'abord importer la
 * classe en question de son fichier
 */






const tileSize = 100;

//On fait une instance de personnage
let perso1 = new _Character__WEBPACK_IMPORTED_MODULE_0__["Character"]('Link');

// On met dans un taleau toutes les instances créees (Character & Item)
const gameObjects = [
    perso1,
    new _Potion__WEBPACK_IMPORTED_MODULE_4__["Potion"]('Health Potion', 2, 2, 20),
    new _Food__WEBPACK_IMPORTED_MODULE_3__["Food"]('Magic Taco', 5, 5, 50),
    new _Item__WEBPACK_IMPORTED_MODULE_2__["Item"]('Shoes', 6, 3, 'shoes')
];
// On fait une boucle pour initialiser le draw de toutes les instances
for (const iterator of gameObjects) {
    document.body.appendChild(iterator.draw());

}

//on fait une instance des contrôles au clavier en lui
//donnant l'instance de personnage à contrôler
let arrowKey = new _controls_ArrowKey__WEBPACK_IMPORTED_MODULE_1__["ArrowKey"](perso1);
//On initialise les contrôles au clavier
arrowKey.init();

// Test pour la méthode .takeItem de Character
// perso1.takeItem();

// Test pour la méthode .useItem de Character
// perso1.useItem(new Item('some stuff'));

// // Test pour Potion
// gameObjects[1].use(perso1);

// // Test pour Food
// gameObjects[2].use(perso1);

/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAiLCJ3ZWJwYWNrOi8vLy4vc3JjL0NoYXJhY3Rlci5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvRm9vZC5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvSXRlbS5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvUG90aW9uLmpzIiwid2VicGFjazovLy8uL3NyYy9jb250cm9scy9BcnJvd0tleS5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvaW5kZXguanMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtRQUFBO1FBQ0E7O1FBRUE7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTs7UUFFQTtRQUNBOztRQUVBO1FBQ0E7O1FBRUE7UUFDQTtRQUNBOzs7UUFHQTtRQUNBOztRQUVBO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0EsMENBQTBDLGdDQUFnQztRQUMxRTtRQUNBOztRQUVBO1FBQ0E7UUFDQTtRQUNBLHdEQUF3RCxrQkFBa0I7UUFDMUU7UUFDQSxpREFBaUQsY0FBYztRQUMvRDs7UUFFQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0EseUNBQXlDLGlDQUFpQztRQUMxRSxnSEFBZ0gsbUJBQW1CLEVBQUU7UUFDckk7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7UUFDQSwyQkFBMkIsMEJBQTBCLEVBQUU7UUFDdkQsaUNBQWlDLGVBQWU7UUFDaEQ7UUFDQTtRQUNBOztRQUVBO1FBQ0Esc0RBQXNELCtEQUErRDs7UUFFckg7UUFDQTs7O1FBR0E7UUFDQTs7Ozs7Ozs7Ozs7OztBQ2xGQTtBQUFBO0FBQUE7QUFBQTtBQUEwQztBQUNaOztBQUU5QjtBQUNBO0FBQ0E7QUFDQTtBQUNPOztBQUVQO0FBQ0EsZUFBZSxPQUFPO0FBQ3RCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0EsZUFBZSxPQUFPO0FBQ3RCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTtBQUNBLGVBQWUsS0FBSztBQUNwQjs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxpQ0FBaUMsNkNBQVc7QUFDNUMsc0NBQXNDLDBDQUFJO0FBQzFDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSwyQkFBMkIsVUFBVSxTQUFTLFVBQVU7QUFDeEQ7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsZUFBZSxLQUFLO0FBQ3BCO0FBQ0E7QUFDQTtBQUNBOzs7QUFHQTtBQUNBO0FBQ0EsaUJBQWlCLFFBQVE7QUFDekI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQSxtREFBbUQsMENBQVE7QUFDM0Qsb0RBQW9ELDBDQUFROztBQUU1RDs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0Esc0NBQXNDLFVBQVU7QUFDaEQ7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQzs7Ozs7Ozs7Ozs7O0FDOUtBO0FBQUE7QUFBQTtBQUFBO0FBQThCO0FBQ1U7O0FBRXhDOztBQUVPLG1CQUFtQiwwQ0FBSTtBQUM5QjtBQUNBO0FBQ0EsZUFBZSxPQUFPO0FBQ3RCLGVBQWUsT0FBTztBQUN0QixlQUFlLE9BQU87QUFDdEIsZUFBZSxPQUFPO0FBQ3RCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsZUFBZSxVQUFVO0FBQ3pCO0FBQ0E7QUFDQTtBQUNBLDJCQUEyQixlQUFlO0FBQzFDO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSx1QkFBdUIsZUFBZSxRQUFRLFVBQVUseUJBQXlCLGlCQUFpQjs7QUFFbEc7QUFDQTtBQUNBO0FBQ0EsQzs7Ozs7Ozs7Ozs7O0FDM0NBO0FBQUE7QUFBQTtBQUFBO0FBQXdDO0FBQ1g7O0FBRXRCO0FBQ1A7QUFDQTtBQUNBLGVBQWUsT0FBTztBQUN0QixlQUFlLE9BQU87QUFDdEIsZUFBZSxPQUFPO0FBQ3RCLGVBQWUsT0FBTztBQUN0QjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxtQkFBbUIsVUFBVTtBQUM3QixxQkFBcUIsT0FBTztBQUM1QjtBQUNBO0FBQ0EsdUJBQXVCLGVBQWUsUUFBUSxVQUFVO0FBQ3hEOztBQUVBO0FBQ0E7QUFDQSxpQkFBaUIsSUFBSTtBQUNyQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQSwwQ0FBMEMsMENBQVE7QUFDbEQsMkNBQTJDLDBDQUFROztBQUVuRDs7QUFFQTs7QUFFQSxDOzs7Ozs7Ozs7Ozs7QUNyREE7QUFBQTtBQUFBO0FBQUE7QUFBOEI7QUFDVTs7O0FBR2pDLHFCQUFxQiwwQ0FBSTtBQUNoQztBQUNBO0FBQ0EsZUFBZSxPQUFPO0FBQ3RCLGVBQWUsT0FBTztBQUN0QixlQUFlLE9BQU87QUFDdEIsZUFBZSxPQUFPO0FBQ3RCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLGVBQWUsVUFBVTtBQUN6QjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLDJCQUEyQixlQUFlLFNBQVMsVUFBVSx5QkFBeUIsaUJBQWlCO0FBQ3ZHOztBQUVBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQSxDOzs7Ozs7Ozs7Ozs7QUNsQ0E7QUFBQTtBQUFBO0FBQXlDOztBQUVsQzs7QUFFUDtBQUNBLGVBQWUsVUFBVTtBQUN6QjtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQWE7QUFDYjtBQUNBLGFBQWE7QUFDYjtBQUNBLGFBQWE7QUFDYjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQSxTQUFTO0FBQ1Q7QUFDQSxDOzs7Ozs7Ozs7Ozs7QUNyQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDd0M7QUFDTztBQUNqQjtBQUNBO0FBQ0k7O0FBRTNCOztBQUVQO0FBQ0EsaUJBQWlCLG9EQUFTOztBQUUxQjtBQUNPO0FBQ1A7QUFDQSxRQUFRLDhDQUFNO0FBQ2QsUUFBUSwwQ0FBSTtBQUNaLFFBQVEsMENBQUk7QUFDWjtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0EsbUJBQW1CLDJEQUFRO0FBQzNCO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQSw4QiIsImZpbGUiOiJtYWluLmpzIiwic291cmNlc0NvbnRlbnQiOlsiIFx0Ly8gVGhlIG1vZHVsZSBjYWNoZVxuIFx0dmFyIGluc3RhbGxlZE1vZHVsZXMgPSB7fTtcblxuIFx0Ly8gVGhlIHJlcXVpcmUgZnVuY3Rpb25cbiBcdGZ1bmN0aW9uIF9fd2VicGFja19yZXF1aXJlX18obW9kdWxlSWQpIHtcblxuIFx0XHQvLyBDaGVjayBpZiBtb2R1bGUgaXMgaW4gY2FjaGVcbiBcdFx0aWYoaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0pIHtcbiBcdFx0XHRyZXR1cm4gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0uZXhwb3J0cztcbiBcdFx0fVxuIFx0XHQvLyBDcmVhdGUgYSBuZXcgbW9kdWxlIChhbmQgcHV0IGl0IGludG8gdGhlIGNhY2hlKVxuIFx0XHR2YXIgbW9kdWxlID0gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0gPSB7XG4gXHRcdFx0aTogbW9kdWxlSWQsXG4gXHRcdFx0bDogZmFsc2UsXG4gXHRcdFx0ZXhwb3J0czoge31cbiBcdFx0fTtcblxuIFx0XHQvLyBFeGVjdXRlIHRoZSBtb2R1bGUgZnVuY3Rpb25cbiBcdFx0bW9kdWxlc1ttb2R1bGVJZF0uY2FsbChtb2R1bGUuZXhwb3J0cywgbW9kdWxlLCBtb2R1bGUuZXhwb3J0cywgX193ZWJwYWNrX3JlcXVpcmVfXyk7XG5cbiBcdFx0Ly8gRmxhZyB0aGUgbW9kdWxlIGFzIGxvYWRlZFxuIFx0XHRtb2R1bGUubCA9IHRydWU7XG5cbiBcdFx0Ly8gUmV0dXJuIHRoZSBleHBvcnRzIG9mIHRoZSBtb2R1bGVcbiBcdFx0cmV0dXJuIG1vZHVsZS5leHBvcnRzO1xuIFx0fVxuXG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlcyBvYmplY3QgKF9fd2VicGFja19tb2R1bGVzX18pXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm0gPSBtb2R1bGVzO1xuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZSBjYWNoZVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5jID0gaW5zdGFsbGVkTW9kdWxlcztcblxuIFx0Ly8gZGVmaW5lIGdldHRlciBmdW5jdGlvbiBmb3IgaGFybW9ueSBleHBvcnRzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQgPSBmdW5jdGlvbihleHBvcnRzLCBuYW1lLCBnZXR0ZXIpIHtcbiBcdFx0aWYoIV9fd2VicGFja19yZXF1aXJlX18ubyhleHBvcnRzLCBuYW1lKSkge1xuIFx0XHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBuYW1lLCB7IGVudW1lcmFibGU6IHRydWUsIGdldDogZ2V0dGVyIH0pO1xuIFx0XHR9XG4gXHR9O1xuXG4gXHQvLyBkZWZpbmUgX19lc01vZHVsZSBvbiBleHBvcnRzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnIgPSBmdW5jdGlvbihleHBvcnRzKSB7XG4gXHRcdGlmKHR5cGVvZiBTeW1ib2wgIT09ICd1bmRlZmluZWQnICYmIFN5bWJvbC50b1N0cmluZ1RhZykge1xuIFx0XHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBTeW1ib2wudG9TdHJpbmdUYWcsIHsgdmFsdWU6ICdNb2R1bGUnIH0pO1xuIFx0XHR9XG4gXHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCAnX19lc01vZHVsZScsIHsgdmFsdWU6IHRydWUgfSk7XG4gXHR9O1xuXG4gXHQvLyBjcmVhdGUgYSBmYWtlIG5hbWVzcGFjZSBvYmplY3RcbiBcdC8vIG1vZGUgJiAxOiB2YWx1ZSBpcyBhIG1vZHVsZSBpZCwgcmVxdWlyZSBpdFxuIFx0Ly8gbW9kZSAmIDI6IG1lcmdlIGFsbCBwcm9wZXJ0aWVzIG9mIHZhbHVlIGludG8gdGhlIG5zXG4gXHQvLyBtb2RlICYgNDogcmV0dXJuIHZhbHVlIHdoZW4gYWxyZWFkeSBucyBvYmplY3RcbiBcdC8vIG1vZGUgJiA4fDE6IGJlaGF2ZSBsaWtlIHJlcXVpcmVcbiBcdF9fd2VicGFja19yZXF1aXJlX18udCA9IGZ1bmN0aW9uKHZhbHVlLCBtb2RlKSB7XG4gXHRcdGlmKG1vZGUgJiAxKSB2YWx1ZSA9IF9fd2VicGFja19yZXF1aXJlX18odmFsdWUpO1xuIFx0XHRpZihtb2RlICYgOCkgcmV0dXJuIHZhbHVlO1xuIFx0XHRpZigobW9kZSAmIDQpICYmIHR5cGVvZiB2YWx1ZSA9PT0gJ29iamVjdCcgJiYgdmFsdWUgJiYgdmFsdWUuX19lc01vZHVsZSkgcmV0dXJuIHZhbHVlO1xuIFx0XHR2YXIgbnMgPSBPYmplY3QuY3JlYXRlKG51bGwpO1xuIFx0XHRfX3dlYnBhY2tfcmVxdWlyZV9fLnIobnMpO1xuIFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkobnMsICdkZWZhdWx0JywgeyBlbnVtZXJhYmxlOiB0cnVlLCB2YWx1ZTogdmFsdWUgfSk7XG4gXHRcdGlmKG1vZGUgJiAyICYmIHR5cGVvZiB2YWx1ZSAhPSAnc3RyaW5nJykgZm9yKHZhciBrZXkgaW4gdmFsdWUpIF9fd2VicGFja19yZXF1aXJlX18uZChucywga2V5LCBmdW5jdGlvbihrZXkpIHsgcmV0dXJuIHZhbHVlW2tleV07IH0uYmluZChudWxsLCBrZXkpKTtcbiBcdFx0cmV0dXJuIG5zO1xuIFx0fTtcblxuIFx0Ly8gZ2V0RGVmYXVsdEV4cG9ydCBmdW5jdGlvbiBmb3IgY29tcGF0aWJpbGl0eSB3aXRoIG5vbi1oYXJtb255IG1vZHVsZXNcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubiA9IGZ1bmN0aW9uKG1vZHVsZSkge1xuIFx0XHR2YXIgZ2V0dGVyID0gbW9kdWxlICYmIG1vZHVsZS5fX2VzTW9kdWxlID9cbiBcdFx0XHRmdW5jdGlvbiBnZXREZWZhdWx0KCkgeyByZXR1cm4gbW9kdWxlWydkZWZhdWx0J107IH0gOlxuIFx0XHRcdGZ1bmN0aW9uIGdldE1vZHVsZUV4cG9ydHMoKSB7IHJldHVybiBtb2R1bGU7IH07XG4gXHRcdF9fd2VicGFja19yZXF1aXJlX18uZChnZXR0ZXIsICdhJywgZ2V0dGVyKTtcbiBcdFx0cmV0dXJuIGdldHRlcjtcbiBcdH07XG5cbiBcdC8vIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbFxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5vID0gZnVuY3Rpb24ob2JqZWN0LCBwcm9wZXJ0eSkgeyByZXR1cm4gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKG9iamVjdCwgcHJvcGVydHkpOyB9O1xuXG4gXHQvLyBfX3dlYnBhY2tfcHVibGljX3BhdGhfX1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5wID0gXCJcIjtcblxuXG4gXHQvLyBMb2FkIGVudHJ5IG1vZHVsZSBhbmQgcmV0dXJuIGV4cG9ydHNcbiBcdHJldHVybiBfX3dlYnBhY2tfcmVxdWlyZV9fKF9fd2VicGFja19yZXF1aXJlX18ucyA9IFwiLi9zcmMvaW5kZXguanNcIik7XG4iLCJpbXBvcnQgeyB0aWxlU2l6ZSwgZ2FtZU9iamVjdHMgfSBmcm9tIFwiLlwiO1xuaW1wb3J0IHsgSXRlbSB9IGZyb20gXCIuL0l0ZW1cIjtcblxuLyoqXG4gKiBQb3VyIHF1ZSBjZXR0ZSBjbGFzc2Ugc29pdCBpbXBvcnRhYmxlIGRhbnMgdW4gYXV0cmVcbiAqIGZpY2hpZXIsIG9uIGRvaXQgbCdpbmRpcXVlciBhdmVjIGxlIG1vdCBjbGVmIGV4cG9ydFxuICovXG5leHBvcnQgY2xhc3MgQ2hhcmFjdGVyIHtcblxuICAgIC8qKlxuICAgICAqIEBwYXJhbSB7c3RyaW5nfSBuYW1lIFxuICAgICAqL1xuICAgIGNvbnN0cnVjdG9yKG5hbWUpIHtcbiAgICAgICAgdGhpcy5uYW1lID0gbmFtZTtcbiAgICAgICAgdGhpcy5oZWFsdGggPSAxMDA7XG4gICAgICAgIHRoaXMuaHVuZ2VyID0gMDtcbiAgICAgICAgdGhpcy5wb3NpdGlvbiA9IHtcbiAgICAgICAgICAgIHg6IDAsXG4gICAgICAgICAgICB5OiAwXG4gICAgICAgIH07XG4gICAgICAgIHRoaXMuZWxlbWVudCA9IG51bGw7XG4gICAgICAgIC8qKlxuICAgICAgICAgKiBAdHlwZSBJdGVtW11cbiAgICAgICAgICovXG4gICAgICAgIHRoaXMuYmFja3BhY2sgPSBbXTtcbiAgICAgICAgdGhpcy5zaG93QmFja3BhY2sgPSBmYWxzZTtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBNw6l0aG9kZSBxdWkgb3V2cmUgb3UgZmVybWUgbGUgYmFja3BhY2sgZHUgcGVyc29ubmFnZVxuICAgICAqL1xuICAgIHRvZ2dsZUJhY2twYWNrKCkge1xuICAgICAgICB0aGlzLnNob3dCYWNrcGFjayA9ICF0aGlzLnNob3dCYWNrcGFjaztcbiAgICAgICAgdGhpcy5kcmF3KCk7XG5cbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBNZXRob2Qgd2hpY2ggaXMgbW9vdmluZyB0aGUgY2hhcmFjdGVyIGluIGEgc3BlY2lmaWMgZGlyZWN0aW9uXG4gICAgICogQHBhcmFtIHtzdHJpbmd9IGRpcmVjdGlvbiB0aGUgZGlyZWN0aW9uIHdoZXJlIHdlIHdhbnQgdG8gbW9vdmUgKG11c3QgYmUgJ3VwJywgJ2Rvd24nLCAnbGVmdCcgb3UgJ3JpZ2h0JylcbiAgICAgKi9cbiAgICBtb3ZlKGRpcmVjdGlvbikge1xuICAgICAgICAvLyAyIHdheXMgdG8gbWFrZSB0aGUgY29uZGl0aW9uIGxvb3AgZm9yIHRoZSBtb3ZtZW50ICh3aXRoIDQgJ2lmJyBvcik6XG4gICAgICAgIHN3aXRjaCAoZGlyZWN0aW9uKSB7XG4gICAgICAgICAgICBjYXNlICd1cCc6XG4gICAgICAgICAgICAgICAgdGhpcy5wb3NpdGlvbi55LS07XG4gICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICBjYXNlICdkb3duJzpcbiAgICAgICAgICAgICAgICB0aGlzLnBvc2l0aW9uLnkrKztcbiAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgIGNhc2UgJ2xlZnQnOlxuICAgICAgICAgICAgICAgIHRoaXMucG9zaXRpb24ueC0tO1xuICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgY2FzZSAncmlnaHQnOlxuICAgICAgICAgICAgICAgIHRoaXMucG9zaXRpb24ueCsrO1xuICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICB9XG4gICAgICAgIHRoaXMubWV0YWJvbGlzbSgpO1xuICAgICAgICB0aGlzLmRyYXcoKTtcbiAgICB9XG5cblxuICAgIC8qKlxuICAgICAqIE1ldGhvZCB0byBtYWtlIHRoYXQgd2hlbiB0aGUgY2hhcmFjdGVyIG1vdmVzLCBodW5nZXIgZGVjcmVtZW50IG9mIDJcbiAgICAgKiBhbmQgaWYgaHVuZ2VyIGlzIHVwIHRvIDEwMCwgaGVhbHRoIHdpbGwgZGVjcmVtZW50LlxuICAgICAqL1xuICAgIG1ldGFib2xpc20oKSB7XG4gICAgICAgIGlmICh0aGlzLmh1bmdlciA8IDEwMCkge1xuICAgICAgICAgICAgdGhpcy5odW5nZXIgKz0gMjtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIHRoaXMuaGVhbHRoLS07XG4gICAgICAgIH1cbiAgICB9XG5cblxuICAgIC8qKlxuICAgICAqIE3DqXRob2RlIHF1aSBhdHRlbmRyYSB1biBpdGVtIGVuIGFyZ3VtZW50IGV0IHF1aSBsJ2Fqb3V0ZXJhIGRhbnMgbGUgdGFibGVhdSBiYWNrcGFja1xuICAgICAqIEBwYXJhbSB7SXRlbX0gaXRlbSBsJ2l0ZW0gYSBtZXR0cmUgZGFucyBsZSBzYWNcbiAgICAgKi9cblxuICAgIHRha2VJdGVtKGl0ZW0pIHtcbiAgICAgICAgLy8gbGV0IGl0ZW07XG4gICAgICAgIGlmICghaXRlbSkge1xuICAgICAgICAgICAgZm9yIChjb25zdCBvYmplY3Qgb2YgZ2FtZU9iamVjdHMpIHtcbiAgICAgICAgICAgICAgICBpZiAob2JqZWN0IGluc3RhbmNlb2YgSXRlbSkge1xuICAgICAgICAgICAgICAgICAgICBpZiAodGhpcy5wb3NpdGlvbi54ID09IG9iamVjdC5wb3NpdGlvbi54ICYmXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnBvc2l0aW9uLnkgPT0gb2JqZWN0LnBvc2l0aW9uLnkpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGl0ZW0gPSBvYmplY3Q7XG4gICAgICAgICAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgICBpZiAoaXRlbSkge1xuICAgICAgICAgICAgdGhpcy5iYWNrcGFjay5wdXNoKGl0ZW0pO1xuICAgICAgICAgICAgLy8gcG91ciBlbmxldmVyIGwnaXRlbSBvbiBkb2l0IG1ldHRyZSBzYSBwb3NpdGlvbiDDoCBudWxsIChzaW5vbiBpbCBkaXNwYXJhaXQgbWFpcyBnYXJkZSBzZXMgY29vcmRvbm7DqWVzKVxuICAgICAgICAgICAgaXRlbS5wb3NpdGlvbi54ID0gbnVsbDtcbiAgICAgICAgICAgIGl0ZW0ucG9zaXRpb24ueSA9IG51bGw7XG4gICAgICAgICAgICBpdGVtLmVsZW1lbnQucmVtb3ZlKCk7XG4gICAgICAgICAgICBjb25zb2xlLmxvZyhgJHt0aGlzLm5hbWV9IHRha2VzICR7aXRlbS5uYW1lfWApO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogTcOpdGhvZGUgcGVybWV0dGFudCBhdSBwZXJzb25uYWdlIGQndXRpbGlzZXIgdW4gaXRlbVxuICAgICAqIEBwYXJhbSB7SXRlbX0gaXRlbSBMJ2l0ZW0gw6AgdXRpbGlzZXJcbiAgICAgKi9cbiAgICB1c2VJdGVtKGl0ZW0pIHtcbiAgICAgICAgaXRlbS51c2UodGhpcyk7XG4gICAgfVxuXG5cbiAgICAvKipcbiAgICAgKiBNw6l0aG9kZSBxdWkgY3LDqWUgbCfDqWzDqW1lbnQgSFRNTCBkdSBwZXJzb25uYWdlXG4gICAgICogQHJldHVybnMge0VsZW1lbnR9IGwnw6lsw6ltZW50IEhUTUwgcmVwcsOpc2VudGFudCBsZSBwZXJzb25uYWdlXG4gICAgICovXG4gICAgZHJhdygpIHtcbiAgICAgICAgLy8gSWYgZWxlbWVudCBpcyBlbXB0eSwgd2UgY3JlYXRlIGl0IDpcbiAgICAgICAgaWYgKCF0aGlzLmVsZW1lbnQpIHtcbiAgICAgICAgICAgIHRoaXMuZWxlbWVudCA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ2RpdicpO1xuICAgICAgICAgICAgdGhpcy5lbGVtZW50LmNsYXNzTGlzdC5hZGQoJ2NoYXJhY3Rlci1zdHlsZScpO1xuICAgICAgICB9IC8vIEVsc2UsIHdlIGp1c3QgaGF2ZSB0byBlbXB0eSB0aGUgcHJldmlvdXMgb25lIDpcbiAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICB0aGlzLmVsZW1lbnQgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcuY2hhcmFjdGVyLXN0eWxlJyk7XG4gICAgICAgICAgICB0aGlzLmVsZW1lbnQuaW5uZXJIVE1MID0gJyc7XG4gICAgICAgIH1cblxuICAgICAgICAvLyBNYWtlIHRoZSBjaGFyYWN0ZXIncyBwYXJhbWV0ZXJzIGFwcGVhciBpbiBpdCA6XG4gICAgICAgIHRoaXMuZHJhd1N0YXRzKCk7XG5cbiAgICAgICAgLy8gQ29uZGl0aW9uIHF1aSBvdXZyZSBsZSBiYWNrcGFjayBzaSBzaG93QmFja3BhY2sgZXN0IHRydWVcbiAgICAgICAgaWYgKHRoaXMuc2hvd0JhY2twYWNrKSB7XG4gICAgICAgICAgICB0aGlzLm9wZW5CYWNrcGFjaygpO1xuICAgICAgICB9XG5cbiAgICAgICAgLy8gVGhlIGNoYXJhY3RlciBtb3ZlcyBpbiBvcmRlciBvZiBpdHMgc2l6ZVxuICAgICAgICB0aGlzLmVsZW1lbnQuc3R5bGUudG9wID0gdGhpcy5wb3NpdGlvbi55ICogdGlsZVNpemUgKyAncHgnO1xuICAgICAgICB0aGlzLmVsZW1lbnQuc3R5bGUubGVmdCA9IHRoaXMucG9zaXRpb24ueCAqIHRpbGVTaXplICsgJ3B4JztcblxuICAgICAgICByZXR1cm4gdGhpcy5lbGVtZW50O1xuXG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogTcOpdGhvZGUgcXVpIGNyw6llIGxlIEhUTUwgZGVzIHN0YXRpc3RpcXVlcyBkdSBwZXJzb25uYWdlXG4gICAgICovXG4gICAgZHJhd1N0YXRzKCkge1xuICAgICAgICBsZXQgcE5hbWUgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdwJyk7XG4gICAgICAgIHBOYW1lLnRleHRDb250ZW50ID0gJ05hbWUgOiAnICsgdGhpcy5uYW1lO1xuICAgICAgICB0aGlzLmVsZW1lbnQuYXBwZW5kQ2hpbGQocE5hbWUpO1xuICAgICAgICBsZXQgcEhlYWx0aCA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ3AnKTtcbiAgICAgICAgcEhlYWx0aC50ZXh0Q29udGVudCA9ICdIZWFsdGggOiAnICsgdGhpcy5oZWFsdGg7XG4gICAgICAgIHRoaXMuZWxlbWVudC5hcHBlbmRDaGlsZChwSGVhbHRoKTtcbiAgICAgICAgbGV0IHBIdW5nZXIgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdwJyk7XG4gICAgICAgIHBIdW5nZXIudGV4dENvbnRlbnQgPSAnSHVuZ2VyIDogJyArIHRoaXMuaHVuZ2VyO1xuICAgICAgICB0aGlzLmVsZW1lbnQuYXBwZW5kQ2hpbGQocEh1bmdlcik7XG4gICAgfVxuXG4gICAgb3BlbkJhY2twYWNrKCkge1xuICAgICAgICBsZXQgYmFja3BhY2sgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdkaXYnKTtcbiAgICAgICAgYmFja3BhY2suY2xhc3NMaXN0LmFkZCgnYmFja3BhY2snKTtcbiAgICAgICAgZm9yIChjb25zdCBpdGVtIG9mIHRoaXMuYmFja3BhY2spIHtcbiAgICAgICAgICAgIC8vIE3DqXRob2RlIHBvdXIgZGVzc2luZXIgbCfDqWzDqW1lbnQgSFRNTCBkdSBiYWNrcGFja1xuICAgICAgICAgICAgLy8gT24gcGV1dCBmYWlyZSBkZSAyIGZhw6dvbnNcbiAgICAgICAgICAgIGxldCBwYXJhSXRlbSA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ3AnKTtcbiAgICAgICAgICAgIHBhcmFJdGVtLnRleHRDb250ZW50ID0gYCR7aXRlbS5uYW1lfWA7XG4gICAgICAgICAgICBwYXJhSXRlbS5hZGRFdmVudExpc3RlbmVyKCdjbGljaycsIGUgPT4gdGhpcy51c2VJdGVtKGl0ZW0pKTtcbiAgICAgICAgICAgIGJhY2twYWNrLmFwcGVuZENoaWxkKHBhcmFJdGVtKTtcbiAgICAgICAgICAgIHRoaXMuZWxlbWVudC5hcHBlbmRDaGlsZChiYWNrcGFjayk7XG5cbiAgICAgICAgICAgIC8vIDJlIG1ldGhvZGUgOiBhIHZlcmlmaWVyXG4gICAgICAgICAgICAvLyBiYWNrcGFjay5hcHBlbmRDaGlsZChpdGVtLmRyYXcoKSk7XG4gICAgICAgIH1cbiAgICB9XG59IiwiaW1wb3J0IHsgSXRlbSB9IGZyb20gXCIuL0l0ZW1cIjtcbmltcG9ydCB7IENoYXJhY3RlciB9IGZyb20gXCIuL0NoYXJhY3RlclwiO1xuXG4vLyBvbiB1dGlsaXNlIGxlIG1vdC1jbMOpICdleHRlbmRzJyBwb3VyIGRpcmUgcXVlIGxhIGNsYXNzZSBGb29kIGjDqXJpdGUgZGUgbGEgY2xhc3NlIEl0ZW1cblxuZXhwb3J0IGNsYXNzIEZvb2QgZXh0ZW5kcyBJdGVtIHtcbiAgICAvKipcbiAgICAgKiBPbiBjb25zdHJ1aXQgbGEgY2xhc3NlIEZvb2QgYXZlYyBsZXMgbcOqbWVzIHBhcmFtw6h0cmVzIHF1ZSBjZXV4IGRlIGxhIGNsYXNzZSBJdGVtXG4gICAgICogQHBhcmFtIHtzdHJpbmd9IG5hbWUgbGUgbm9tIGRlIGxhIG5vdXJyaXR1cmVcbiAgICAgKiBAcGFyYW0ge251bWJlcn0geCBwb3NpdGlvbiB4IGRlIGxhIG5vdXJyaXR1cmVcbiAgICAgKiBAcGFyYW0ge251bWJlcn0geSBwb3NpdGlvbiB5IGRlIGxhIG5vdXJyaXR1cmVcbiAgICAgKiBAcGFyYW0ge251bWJlcn0gY2Fsb3JpZXMgbGVzIGNhbG9yaWVzIGRlIGxhIG5vdXJyaXR1cmVcbiAgICAgKi9cbiAgICBjb25zdHJ1Y3RvcihuYW1lLCB4LCB5LCBjYWxvcmllcykge1xuICAgICAgICAvLyBBVkFOVCBUT1VUIGlsIGZhdXQgZMOpY2xlbmNoZXIgbGUgY29uc3RydWN0ZXVyIGR1IHBhcmVudCAoaWNpIEl0ZW0pIGF2ZWMgbGUgbW90IGNsw6kgJ3N1cGVyJ1xuICAgICAgICBzdXBlcihuYW1lLCB4LCB5LCAnZm9vZCcpO1xuICAgICAgICB0aGlzLmNhbG9yaWVzID0gY2Fsb3JpZXM7XG4gICAgfVxuXG5cbiAgICAvLyBMYSBjbGFzc2UgZm9vZCBow6lyaXRlIGRlcyBtw6l0aG9kZSBkZSBzYSBjbGFzc2UgcGFyZW50IChJdGVtKSBcbiAgICAvLyBldCBvbiBwZXV0IG1vZGlmaWVyIGxhIG3DqXRob2RlIGFwcGVsw6llIHNww6ljaWZpcXVlbWVudCBcbiAgICAvLyBwb3VyIGxhIGNsYXNzZSBmb29kIChvdmVycmlkZSA9IHJlZMOpZmluaXRpb24gZGUgbGEgbcOpdGhvZGUpXG4gICAgLy8gQydlc3QgZGUgbGEgUE9MWU1PUlBISUUgXG4gICAgLyoqXG4gICAgICogQG92ZXJyaWRlXG4gICAgICogQHBhcmFtIHtDaGFyYWN0ZXJ9IGNoYXJhY3RlclxuICAgICAqL1xuICAgIHVzZShjaGFyYWN0ZXIpIHtcbiAgICAgICAgaWYgKGNoYXJhY3Rlci5odW5nZXIgPT0gMCkge1xuICAgICAgICAgICAgY29uc29sZS5sb2coYCR7Y2hhcmFjdGVyLm5hbWV9IGlzIG5vdCBodW5ncnkgZm9yIG5vdyAhYCk7XG4gICAgICAgICAgICByZXR1cm47XG4gICAgICAgIH1cbiAgICAgICAgY2hhcmFjdGVyLmh1bmdlciAtPSB0aGlzLmNhbG9yaWVzO1xuXG4gICAgICAgIGlmIChjaGFyYWN0ZXIuaHVuZ2VyIC0gdGhpcy5jYWxvcmllcyA8IDApIHtcbiAgICAgICAgICAgIGNoYXJhY3Rlci5odW5nZXIgPSAwO1xuICAgICAgICB9XG4gICAgICAgIGNvbnNvbGUubG9nKGAke2NoYXJhY3Rlci5uYW1lfSBlYXRzICR7dGhpcy5uYW1lfSBhbmQgaGlzIGh1bmdlciBpcyBub3cgJHtjaGFyYWN0ZXIuaHVuZ2VyfWApO1xuXG4gICAgICAgIC8vIGV0IG9uIHJhcHBlbGxlIGxhIG3DqXRob2RlIGR1IHBhcmVudCBwb3VyIHF1J2VsbGUgc29pdCBhdXNzaSBleMOpY3V0w6llXG4gICAgICAgIHJldHVybiBzdXBlci51c2UoY2hhcmFjdGVyKTtcbiAgICB9XG59IiwiaW1wb3J0IHsgQ2hhcmFjdGVyIH0gZnJvbSBcIi4vQ2hhcmFjdGVyXCI7XG5pbXBvcnQgeyB0aWxlU2l6ZSB9IGZyb20gXCIuXCI7XG5cbmV4cG9ydCBjbGFzcyBJdGVtIHtcbiAgICAvKipcbiAgICAgKiBcbiAgICAgKiBAcGFyYW0ge3N0cmluZ30gbmFtZSBsZSBub20gZGUgbCdpdGVtXG4gICAgICogQHBhcmFtIHtudW1iZXJ9IHggcG9zaXRpb24geCBkZSBsJ2l0ZW1cbiAgICAgKiBAcGFyYW0ge251bWJlcn0geSBwb3NpdGlvbiB5IGRlIGwnaXRlbVxuICAgICAqIEBwYXJhbSB7c3RyaW5nfSBjc3NDbGFzcyBsYSBjbGFzc2UgY3NzIHJlcHLDqXNlbnRhbnQgY2V0IGl0ZW1cbiAgICAgKi9cbiAgICBjb25zdHJ1Y3RvcihuYW1lLCB4LCB5LCBjc3NDbGFzcyA9ICdkZWZhdWx0LWNsYXNzJykge1xuICAgICAgICAgICAgdGhpcy5uYW1lID0gbmFtZTtcbiAgICAgICAgICAgIHRoaXMucG9zaXRpb24gPSB7XG4gICAgICAgICAgICAgICAgeCxcbiAgICAgICAgICAgICAgICB5XG4gICAgICAgICAgICB9O1xuICAgICAgICAgICAgdGhpcy5jc3NDbGFzcyA9IGNzc0NsYXNzO1xuICAgICAgICAgICAgLyoqXG4gICAgICAgICAgICAgKiBAdHlwZSBFbGVtZW50XG4gICAgICAgICAgICAgKi9cbiAgICAgICAgICAgIHRoaXMuZWxlbWVudCA9IG51bGw7XG4gICAgICAgIH1cbiAgICAgICAgLyoqXG4gICAgICAgICAqIE3DqXRob2RlIHF1aSBzZXJhIGFwcGVsw6llIHF1YW5kIG9uIHV0aWxpc2UgbCdpdGVtXG4gICAgICAgICAqIEBwYXJhbSB7Q2hhcmFjdGVyfSBjaGFyYWN0ZXIgbGUgcGVyc29ubmFnZSBxdWkgdXRpbGlzZSBsJ2l0ZW0gXG4gICAgICAgICAqIEByZXR1cm5zIHtzdHJpbmd9IExhIGNoYWluZSBkZSBjYXJhY3TDqHJlcyBpbmRpcXVhbnQgbCd1dGlsaXNhdGlvbiBkZSBsJ2l0ZW1cbiAgICAgICAgICovXG4gICAgdXNlKGNoYXJhY3Rlcikge1xuICAgICAgICBjb25zb2xlLmxvZyhgJHtjaGFyYWN0ZXIubmFtZX0gdXNlZCAke3RoaXMubmFtZX0gIWApO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIE3DqXRob2RlIHBlcm1ldHRhbnQgZGUgZGVzaXNuZXIgbCfDqWzDqW1lbnQgc2F1ZiBzJ2lsIGV4aXN0ZSBkw6lqYVxuICAgICAqIEByZXR1cm5zIHtkaXZ9IGwnw6lsw6ltZW50IEhUTUwgcmVwcsOpc2VudGF0biBsJ0l0ZW1cbiAgICAgKi9cbiAgICBkcmF3KCkge1xuICAgICAgICBpZiAoIXRoaXMuZWxlbWVudCkge1xuICAgICAgICAgICAgdGhpcy5lbGVtZW50ID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnZGl2Jyk7XG4gICAgICAgIH1cbiAgICAgICAgbGV0IGRpdiA9IHRoaXMuZWxlbWVudDtcbiAgICAgICAgZGl2LmlubmVySFRNTCA9ICcnO1xuICAgICAgICBkaXYuY2xhc3NMaXN0LmFkZCgnaXRlbScpO1xuICAgICAgICBkaXYuY2xhc3NMaXN0LmFkZCh0aGlzLmNzc0NsYXNzKTtcbiAgICAgICAgZGl2LnRleHRDb250ZW50ID0gJyc7XG5cbiAgICAgICAgZGl2LnN0eWxlLnRvcCA9IHRoaXMucG9zaXRpb24ueSAqIHRpbGVTaXplICsgJ3B4JztcbiAgICAgICAgZGl2LnN0eWxlLmxlZnQgPSB0aGlzLnBvc2l0aW9uLnggKiB0aWxlU2l6ZSArICdweCc7XG5cbiAgICAgICAgcmV0dXJuIGRpdjtcblxuICAgIH1cblxufSIsImltcG9ydCB7IEl0ZW0gfSBmcm9tIFwiLi9JdGVtXCI7XG5pbXBvcnQgeyBDaGFyYWN0ZXIgfSBmcm9tIFwiLi9DaGFyYWN0ZXJcIjtcblxuXG5leHBvcnQgY2xhc3MgUG90aW9uIGV4dGVuZHMgSXRlbSB7XG4gICAgLyoqXG4gICAgICogT24gY29uc3RydWl0IGxhIGNsYXNzZSBQb3Rpb24gYXZlYyBsZXMgbcOqbWVzIHBhcmFtw6h0cmVzIHF1ZSBjZXV4IGRlIGxhIGNsYXNzZSBJdGVtXG4gICAgICogQHBhcmFtIHtzdHJpbmd9IG5hbWUgbGUgbm9tIGRlIGxhIHBvdGlvblxuICAgICAqIEBwYXJhbSB7bnVtYmVyfSB4IHBvc2l0aW9uIHggZGUgbGEgcG90aW9uXG4gICAgICogQHBhcmFtIHtudW1iZXJ9IHkgcG9zaXRpb24geSBkZSBsYSBwb3Rpb25cbiAgICAgKiBAcGFyYW0ge251bWJlcn0gaGVhbGluZyBsZXMgY2Fsb3JpZXMgZGUgbGEgcG90aW9uXG4gICAgICovXG4gICAgY29uc3RydWN0b3IobmFtZSwgeCwgeSwgaGVhbGluZykge1xuICAgICAgICBzdXBlcihuYW1lLCB4LCB5LCAncG90aW9uJyk7XG4gICAgICAgIHRoaXMuaGVhbGluZyA9IGhlYWxpbmc7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogQG92ZXJyaWRlXG4gICAgICogQHBhcmFtIHtDaGFyYWN0ZXJ9IGNoYXJhY3RlclxuICAgICAqL1xuICAgIHVzZShjaGFyYWN0ZXIpIHtcbiAgICAgICAgaWYgKGNoYXJhY3Rlci5oZWFsdGggPCAxMDApIHtcbiAgICAgICAgICAgIGNoYXJhY3Rlci5oZWFsdGggKz0gdGhpcy5oZWFsaW5nO1xuICAgICAgICAgICAgaWYgKGNoYXJhY3Rlci5oZWFsdGggKyB0aGlzLmhlYWxpbmcgPiAxMDApIHtcbiAgICAgICAgICAgICAgICBjaGFyYWN0ZXIuaGVhbHRoID0gMTAwO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgY29uc29sZS5sb2coYCR7Y2hhcmFjdGVyLm5hbWV9IHRha2VzICR7dGhpcy5uYW1lfSBhbmQgaGlzIGhlYWx0aCBpcyBub3cgJHtjaGFyYWN0ZXIuaGVhbHRofS5gKTtcbiAgICAgICAgICAgIHJldHVybiBzdXBlci51c2UoY2hhcmFjdGVyKTtcblxuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgY29uc29sZS5sb2coYFlvdXIgaGVhbHRoIGlzIGFscmVhZHkgbWF4aW11bSAhYCk7XG4gICAgICAgIH1cbiAgICB9XG59IiwiaW1wb3J0IHsgQ2hhcmFjdGVyIH0gZnJvbSBcIi4uL0NoYXJhY3RlclwiO1xuXG5leHBvcnQgY2xhc3MgQXJyb3dLZXkge1xuXG4gICAgLyoqXG4gICAgICogQHBhcmFtIHtDaGFyYWN0ZXJ9IGNoYXJhY3RlclxuICAgICAqL1xuICAgIC8vIFRoZSBuZXcgcHJvcGVydHkgb2YgdGhlIEFycm93S2V5IGNsYXNzIGlzIGFuIGluc3RhbmNlIG9mIENoYXJhY3RlciA6XG5cbiAgICBjb25zdHJ1Y3RvcihjaGFyYWN0ZXIpIHtcbiAgICAgICAgdGhpcy5jaGFyYWN0ZXIgPSBjaGFyYWN0ZXI7XG4gICAgfVxuXG4gICAgaW5pdCgpIHtcbiAgICAgICAgd2luZG93LmFkZEV2ZW50TGlzdGVuZXIoJ2tleWRvd24nLCBldmVudCA9PiB7XG4gICAgICAgICAgICBpZiAoZXZlbnQua2V5Q29kZSA9PT0gMzgpIHtcbiAgICAgICAgICAgICAgICB0aGlzLmNoYXJhY3Rlci5tb3ZlKCd1cCcpO1xuICAgICAgICAgICAgfSBlbHNlIGlmIChldmVudC5rZXlDb2RlID09PSA0MCkge1xuICAgICAgICAgICAgICAgIHRoaXMuY2hhcmFjdGVyLm1vdmUoJ2Rvd24nKTtcbiAgICAgICAgICAgIH0gZWxzZSBpZiAoZXZlbnQua2V5Q29kZSA9PT0gMzcpIHtcbiAgICAgICAgICAgICAgICB0aGlzLmNoYXJhY3Rlci5tb3ZlKCdsZWZ0Jyk7XG4gICAgICAgICAgICB9IGVsc2UgaWYgKGV2ZW50LmtleUNvZGUgPT09IDM5KSB7XG4gICAgICAgICAgICAgICAgdGhpcy5jaGFyYWN0ZXIubW92ZSgncmlnaHQnKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIC8vIFRvdWNoZSBlc3BhY2UgcG91ciBwcmVuZHJlIHVuIG9iamV0XG4gICAgICAgICAgICBpZiAoZXZlbnQua2V5Q29kZSA9PT0gMzIpIHtcbiAgICAgICAgICAgICAgICB0aGlzLmNoYXJhY3Rlci50YWtlSXRlbSgpO1xuICAgICAgICAgICAgICAgIC8vIGNvbnNvbGUubG9nKHRoaXMuY2hhcmFjdGVyLmJhY2twYWNrKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIC8vIFRvdWNoZSBpIHBvdXIgYWZmaWNoZXIgaW52ZW50YWlyZVxuICAgICAgICAgICAgaWYgKGV2ZW50LmtleUNvZGUgPT09IDczKSB7XG4gICAgICAgICAgICAgICAgdGhpcy5jaGFyYWN0ZXIudG9nZ2xlQmFja3BhY2soKTtcblxuICAgICAgICAgICAgfVxuXG4gICAgICAgIH0pO1xuICAgIH1cbn0iLCIvKipcbiAqIEVuIEpTIG1vZHVsYWlyZSwgcG91ciBwb3V2b2lyIHV0aWxpc2VyIGxhIGNsYXNzZSBDaGFyYWN0ZXJcbiAqIGRhbnMgbm90cmUgZmljaGllciBpbmRleCwgb24gZG9pdCBkJ2Fib3JkIGltcG9ydGVyIGxhXG4gKiBjbGFzc2UgZW4gcXVlc3Rpb24gZGUgc29uIGZpY2hpZXJcbiAqL1xuaW1wb3J0IHsgQ2hhcmFjdGVyIH0gZnJvbSBcIi4vQ2hhcmFjdGVyXCI7XG5pbXBvcnQgeyBBcnJvd0tleSB9IGZyb20gXCIuL2NvbnRyb2xzL0Fycm93S2V5XCI7XG5pbXBvcnQgeyBJdGVtIH0gZnJvbSBcIi4vSXRlbVwiO1xuaW1wb3J0IHsgRm9vZCB9IGZyb20gXCIuL0Zvb2RcIjtcbmltcG9ydCB7IFBvdGlvbiB9IGZyb20gXCIuL1BvdGlvblwiO1xuXG5leHBvcnQgY29uc3QgdGlsZVNpemUgPSAxMDA7XG5cbi8vT24gZmFpdCB1bmUgaW5zdGFuY2UgZGUgcGVyc29ubmFnZVxubGV0IHBlcnNvMSA9IG5ldyBDaGFyYWN0ZXIoJ0xpbmsnKTtcblxuLy8gT24gbWV0IGRhbnMgdW4gdGFsZWF1IHRvdXRlcyBsZXMgaW5zdGFuY2VzIGNyw6llZXMgKENoYXJhY3RlciAmIEl0ZW0pXG5leHBvcnQgY29uc3QgZ2FtZU9iamVjdHMgPSBbXG4gICAgcGVyc28xLFxuICAgIG5ldyBQb3Rpb24oJ0hlYWx0aCBQb3Rpb24nLCAyLCAyLCAyMCksXG4gICAgbmV3IEZvb2QoJ01hZ2ljIFRhY28nLCA1LCA1LCA1MCksXG4gICAgbmV3IEl0ZW0oJ1Nob2VzJywgNiwgMywgJ3Nob2VzJylcbl07XG4vLyBPbiBmYWl0IHVuZSBib3VjbGUgcG91ciBpbml0aWFsaXNlciBsZSBkcmF3IGRlIHRvdXRlcyBsZXMgaW5zdGFuY2VzXG5mb3IgKGNvbnN0IGl0ZXJhdG9yIG9mIGdhbWVPYmplY3RzKSB7XG4gICAgZG9jdW1lbnQuYm9keS5hcHBlbmRDaGlsZChpdGVyYXRvci5kcmF3KCkpO1xuXG59XG5cbi8vb24gZmFpdCB1bmUgaW5zdGFuY2UgZGVzIGNvbnRyw7RsZXMgYXUgY2xhdmllciBlbiBsdWlcbi8vZG9ubmFudCBsJ2luc3RhbmNlIGRlIHBlcnNvbm5hZ2Ugw6AgY29udHLDtGxlclxubGV0IGFycm93S2V5ID0gbmV3IEFycm93S2V5KHBlcnNvMSk7XG4vL09uIGluaXRpYWxpc2UgbGVzIGNvbnRyw7RsZXMgYXUgY2xhdmllclxuYXJyb3dLZXkuaW5pdCgpO1xuXG4vLyBUZXN0IHBvdXIgbGEgbcOpdGhvZGUgLnRha2VJdGVtIGRlIENoYXJhY3RlclxuLy8gcGVyc28xLnRha2VJdGVtKCk7XG5cbi8vIFRlc3QgcG91ciBsYSBtw6l0aG9kZSAudXNlSXRlbSBkZSBDaGFyYWN0ZXJcbi8vIHBlcnNvMS51c2VJdGVtKG5ldyBJdGVtKCdzb21lIHN0dWZmJykpO1xuXG4vLyAvLyBUZXN0IHBvdXIgUG90aW9uXG4vLyBnYW1lT2JqZWN0c1sxXS51c2UocGVyc28xKTtcblxuLy8gLy8gVGVzdCBwb3VyIEZvb2Rcbi8vIGdhbWVPYmplY3RzWzJdLnVzZShwZXJzbzEpOyJdLCJzb3VyY2VSb290IjoiIn0=